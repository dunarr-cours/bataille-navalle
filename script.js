const plateauElement = document.getElementById('plateau')
const messageElement = document.getElementById("message")
const counterElement = document.getElementById("counter")
const launchButtonElement = document.getElementById("jouer")
var places = []
var win = false
var boat
var counter = 0

launchButtonElement.addEventListener('click',launchGame)

function launchGame(){
    boat = Math.floor(Math.random()*16)
    for (let i = 0; i < 16; i++) {
        places[i] = false
    }
    counter = 0
    win = false
    display()
}

function tryCase(caseId){
    var place = places[caseId]
    if (!place && !win) {
        places[caseId] = true
        counter++
        if (caseId ==boat){
            win = true
        }
        display()
    }
}

function display(){
    counterElement.innerHTML = `nombre de tentatives: ${counter}`
    plateauElement.innerHTML = ""
    if(win){
        messageElement.innerHTML = "Victoire!!"
        launchButtonElement.style.display = ""
    } else {
        messageElement.innerHTML = "Pourrez-vous trouver où se trouve le bateau ?"
        launchButtonElement.style.display = "none"
    }
    for (let place in places){
        var caseElement = document.createElement("div")
        caseElement.classList.add("case")
        if (places[place]){
            if (place == boat){
                caseElement.classList.add('touched')
            } else {
                caseElement.classList.add("failed")
            }
        }
        caseElement.addEventListener("click", function (){
            tryCase(place)
        })
        plateauElement.appendChild(caseElement)
    }
}

